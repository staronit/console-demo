var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
var less = require('gulp-less');
var path = require('path');
var paths = {
    pages: ['src/*.html'],
    assets: ['src/assets/**/*']
};

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
        .pipe(gulp.dest("dist"));
});

gulp.task("copy-assets", function () {
    return gulp.src(paths.assets)
        .pipe(gulp.dest("dist/assets"));
});

gulp.task("less", function() {
    return gulp.src('./src/less/main.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'src', 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('ts', function(){
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/scripts/main.ts'],
        cache: {},
        packageCache: {}
    })
    .plugin(tsify)
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest("dist"));
});

gulp.task('watch',function() {
    gulp.watch(['./src/scripts/**/*.ts'],['ts']);
    gulp.watch(['./src/less/**/*.less'], ['less']);
    gulp.watch(['./src/**/*.html'], ['copy-html']);
});

gulp.task("default", ["copy-html", "copy-assets", "less", "ts"]);