import { Service } from "typedi";

@Service("input.prompt")
export default class InputPrompt {

    private before: HTMLElement;
    private after: HTMLElement;
    private cursor: HTMLElement;

    private characters: string[] = [];
    private index: number = 0;

    constructor() {
        this.before = <HTMLElement>document.querySelector("#terminal-wrapper .input .before");
        this.cursor = <HTMLElement>document.querySelector("#terminal-wrapper .input .cursor");
        this.after = <HTMLElement>document.querySelector("#terminal-wrapper .input .after");
    }

    setCommandText(commandText: string): void {
        this.characters = commandText.split("").map((character: string): string => {
            return character.replace(/\s/g, "&nbsp;");
        });
        this.index = this.characters.length;

        this.render();
    }

    getCommandText(): string {
        return this.characters.join("").replace("&nbsp;", " ");
    }

    add(character: string): void {
        this.characters.splice(this.index, 0, character);
        this.index++;

        this.render();
    }

    moveLeft(): void {
        if (this.index - 1 > -1) {
            this.index--;
        }

        this.render();
    }

    moveRight(): void {
        if (this.index + 1 <= this.characters.length) {
            this.index++;
        }

        this.render();
    }

    backspace(): void {
        if (this.index > 0) {
            this.characters.splice(this.index - 1, 1);
            this.index--;
            this.render();
        }
    }

    delete(): void {
        if (this.index > 0) {
            this.characters.splice(this.index, 1);
            this.render();
        }
    }

    clear(): void {
        this.characters = [];
        this.index = 0;

        this.render();
    }

    private render(): void {
        this.renderList(this.characters.slice(0, this.getBeforeIndex()), this.before);
        this.renderCursor();
        this.renderList(this.characters.slice(this.getAfterIndex()), this.after);
    }

    private getBeforeIndex(): number {
        return this.index;
    }

    private getAfterIndex(): number {
        return this.isCursorOverCharacter() ? this.index + 1 : this.index;
    }

    private renderCursor(): void {
        if (this.isCursorOverCharacter()) {
            this.cursor.innerHTML = this.characters[this.index];
        } else {
            this.cursor.innerHTML = "&nbsp";
        }
    }

    private renderList(characters: string[], element: HTMLElement): void {
        element.innerHTML = "";
        for (const character of characters) {
            const characterElement = document.createElement("span");
            characterElement.innerHTML = character;
            element.appendChild(characterElement);
        }
    }

    private isCursorOverCharacter(): boolean {
        return this.characters.length !== this.index;
    }
}