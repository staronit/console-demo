import {Container, Inject, Service} from "typedi";
import QueryFactory from "../Query/Factory/QueryFactory";
import Query from "../Query/Query";
import InputKey from "./InputKey";
import InputPrompt from "./InputPrompt";
import InputTypeFactoryChain from "./Types/Factory/InputTypeFactoryChain";
import InputTypeFactoryDelete from "./Types/Factory/InputTypeFactoryDelete";
import InputTypeInterface from "./Types/InputTypeInteface";

@Service("input")
export default class Input {

    container: HTMLElement;

    private inputTypeFactory: InputTypeFactoryChain;
    private queryFactory: QueryFactory;
    private prompt: InputPrompt;

    constructor(
        @Inject("input.prompt") prompt: InputPrompt,
        @Inject("query.factory") queryFactory: QueryFactory,
        @Inject("input.type.factory") inputTypeFactory: InputTypeFactoryChain,
    ) {
        this.inputTypeFactory = inputTypeFactory;
        this.prompt = prompt;
        this.queryFactory = queryFactory;

        this.container = <HTMLElement>document.querySelector("#terminal-wrapper .input");
    }

    scroll() {
        window.scrollTo(0, this.container.offsetTop);
    }

    setCommandText(commandText: string): void {
        this.prompt.setCommandText(commandText);
    }

    getQuery(): Query {
        return this.queryFactory.create(this.prompt.getCommandText());
    }

    show(): void {
        document.querySelector("#terminal-wrapper .input").classList.add("active");
    }

    clear(): void {
        this.prompt.clear();
    }

    processKey(key: InputKey): void {
        this.scroll();
        const inputType: InputTypeInterface = this.inputTypeFactory.create(key);
        inputType.process(this.prompt);
    }
}