export default class InputKey {

    static KEY_ENTER = 13;
    static KEY_SPACE = 32;
    static KEY_BACKSPACE = 8;
    static KEY_DELETE = 46;
    static KEY_ARROW_LEFT = 37;
    static KEY_ARROW_RIGHT = 39;
    static KEY_ARROW_UP = 38;
    static KEY_ARROW_DOWN = 40;

    private key: string;
    private code: number;

    constructor(key: string, code: number) {
        this.key = key,
        this.code = code;
    }

    getKey(): string {
        return this.key;
    }

    getCode(): number {
        return this.code;
    }

    isSupported(): boolean {
        return this.isCharacter() ||
            this.isBackspace() ||
            this.isDelete() ||
            this.isArrowLeftRight();
    }

    isEnter(): boolean {
        return this.code === InputKey.KEY_ENTER;
    }

    isCharacter(): boolean {
        return (this.code > 47 && this.code < 58)   || // number keys
            this.code === InputKey.KEY_SPACE      || // spacebar
            (this.code > 64 && this.code < 91)   || // letter keys
            (this.code > 95 && this.code < 112)  || // numpad keys
            (this.code > 185 && this.code < 193) || // ;=,-./` (in order)
            (this.code > 218 && this.code < 223);   // [\]' (in order);
    }

    isBackspace(): boolean {
        return this.code === InputKey.KEY_BACKSPACE;
    }

    isDelete(): boolean {
        return this.code === InputKey.KEY_DELETE;
    }

    isArrowLeftRight(): boolean {
        return this.code === InputKey.KEY_ARROW_LEFT || this.code === InputKey.KEY_ARROW_RIGHT;
    }

    isArrowDown(): boolean {
        return this.code === InputKey.KEY_ARROW_DOWN;
    }

    isArrowUp(): boolean {
        return this.code === InputKey.KEY_ARROW_UP;
    }
}