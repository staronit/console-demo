import InputPrompt from "../InputPrompt";
import InputTypeInterface from "./InputTypeInteface";

export default class InputTypeBackspace implements InputTypeInterface {
    process(prompt: InputPrompt): void {
        prompt.backspace();
    }
}