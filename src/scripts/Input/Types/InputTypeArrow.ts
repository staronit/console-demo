import InputPrompt from "../InputPrompt";
import InputTypeInterface from "./InputTypeInteface";

export default class InputTypeArrow implements InputTypeInterface {
    static LEFT = "left";
    static RIGHT = "right";

    private direction: string;

    constructor(direction: string) {
        this.direction = direction;
    }

    process(prompt: InputPrompt): void {
        switch (this.direction) {
            case InputTypeArrow.LEFT:
                prompt.moveLeft();
                break;
            case InputTypeArrow.RIGHT:
                prompt.moveRight();
                break;
        }
    }
}