import InputPrompt from "../InputPrompt";
import InputTypeInterface from "./InputTypeInteface";

export default class InputTypeDelete implements InputTypeInterface {
    process(prompt: InputPrompt): void {
        prompt.delete();
    }
}