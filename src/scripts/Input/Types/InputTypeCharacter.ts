import InputPrompt from '../InputPrompt';
import InputTypeInterface from './InputTypeInteface';

export default class InputTypeCharacter implements InputTypeInterface {
    private character: string;

    constructor(character: string) {
        this.character = character.replace(/\s/g, '&nbsp;');
    }

    process(prompt: InputPrompt): void {
        prompt.add(this.character);
    }
}