import InputPrompt from "../InputPrompt";

export default interface InputTypeInterface {
    process(prompt: InputPrompt): void;
}