import { Inject, Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryArrow from "./InputTypeFactoryArrow";
import InputTypeFactoryBackspace from "./InputTypeFactoryBackspace";
import InputTypeFactoryCharacter from "./InputTypeFactoryCharacter";
import InputTypeFactoryDefault from "./InputTypeFactoryDefault";
import InputTypeFactoryDelete from "./InputTypeFactoryDelete";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory")
export default class InputTypeFactoryChain {

    private factories: InputTypeFactoryInterface[];

    constructor(
        @Inject("input.type.factory.character") inputTypeFactoryCharacter: InputTypeFactoryCharacter,
        @Inject("input.type.factory.backspace") inputTypeFactoryBackspace: InputTypeFactoryBackspace,
        @Inject("input.type.factory.arrow") inputTypeFactoryArrow: InputTypeFactoryArrow,
        @Inject("input.type.factory.delete") inputTypeFactoryDelete: InputTypeFactoryDelete,
        @Inject("input.type.factory.default") inputTypeFactoryDefault: InputTypeFactoryDefault,
    ) {
        this.factories = new Array<InputTypeFactoryInterface>(
            inputTypeFactoryCharacter,
            inputTypeFactoryBackspace,
            inputTypeFactoryArrow,
            inputTypeFactoryDelete,
            inputTypeFactoryDefault,
        );
    }

    create(key: InputKey): InputTypeInterface {
        const factory = this.getFactory(key);
        return factory.create(key);
    }

    addFactory(factory: InputTypeFactoryInterface): void {
        this.factories.push(factory);
    }

    private getFactory(key: InputKey): InputTypeFactoryInterface {
        for (const factory of this.factories) {
            if (factory.canCreate(key)) {
                return factory;
            }
        }

        throw new Error(`InputTypeFactory not found for ${key.getCode()} ${key.getKey()}`);
    }
}