import { Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeDelete from "../InputTypeDelete";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory.delete")
export default class InputTypeFactoryDelete implements InputTypeFactoryInterface {

    canCreate(key: InputKey): boolean {
        return key.isDelete();
    }

    create(key: InputKey): InputTypeInterface {
        return new InputTypeDelete();
    }
}