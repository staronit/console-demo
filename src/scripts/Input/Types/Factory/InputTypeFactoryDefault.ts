import { Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeDefault from "../InputTypeDefault";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory.default")
export default class InputTypeFactoryDefault implements InputTypeFactoryInterface {

    canCreate(key: InputKey): boolean {
        return !key.isSupported();
    }

    create(key: InputKey): InputTypeInterface {
        return new InputTypeDefault();
    }
}