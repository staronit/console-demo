import { Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeBackspace from "../InputTypeBackspace";
import InputTypeCharacter from "../InputTypeCharacter";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory.backspace")
export default class InputTypeFactoryBackspace implements InputTypeFactoryInterface {

    canCreate(key: InputKey): boolean {
        return key.isBackspace();
    }

    create(event: InputKey): InputTypeInterface {
        return new InputTypeBackspace();
    }
}