import InputKey from "../../InputKey";
import InputTypeInterface from "../InputTypeInteface";

export default interface InputTypeFactoryInterface {
    canCreate(key: InputKey): boolean;
    create(key: InputKey): InputTypeInterface;
}