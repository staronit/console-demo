import { Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeArrow from "../InputTypeArrow";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory.arrow")
export default class InputTypeFactoryArrow implements InputTypeFactoryInterface {

    canCreate(key: InputKey): boolean {
        return key.isArrowLeftRight();
    }

    create(key: InputKey): InputTypeInterface {
        return new InputTypeArrow(
            this.getDirection(key.getCode()),
        );
    }

    private getDirection(keyCode: number): string {
        if (keyCode === InputKey.KEY_ARROW_LEFT) {
            return InputTypeArrow.LEFT;
        } else {
            return InputTypeArrow.RIGHT;
        }
    }
}