import { Service } from "typedi";
import InputKey from "../../InputKey";
import InputTypeCharacter from "../InputTypeCharacter";
import InputTypeInterface from "../InputTypeInteface";
import InputTypeFactoryInterface from "./InputTypeFactoryInterface";

@Service("input.type.factory.character")
export default class InputTypeFactoryCharacter implements InputTypeFactoryInterface {

    canCreate(key: InputKey): boolean {
        return key.isCharacter();
    }

    create(key: InputKey): InputTypeInterface {
        return new InputTypeCharacter(key.getKey());
    }
}