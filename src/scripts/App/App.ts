import {Container, Inject, Service} from "typedi";
import CommandInterface from "../Command/CommandInterface";
import Executor from "../Executor/Executor";
import Input from "../Input/Input";
import InputKey from "../Input/InputKey";
import Output from "../Output/Output";
import Query from "../Query/Query";
import LocalStorage from "../Storage/LocalStorage";
import StorageInterface from "../Storage/StorageInterface";

@Service()
export default class App {
    private input: Input;
    private output: Output;
    private executor: Executor;
    private storage: StorageInterface;

    private started: boolean = false;

    constructor(
        @Inject("input") input: Input,
        @Inject("output") output: Output,
        @Inject("executor") executor: Executor,
        @Inject("storage.local_storage") storage: LocalStorage,
    ) {
        this.input = input;
        this.output = output;
        this.executor = executor;
        this.storage = storage;
    }

    start(): void {
        this.intro();
        document.addEventListener("keydown", (event: KeyboardEvent) => {
            if (this.canProcessKey(event)) {
                this.handleKeyDown(
                    new InputKey(event.key, event.keyCode),
                );
            }
        }, false);
    }

    handleKeyDown(key: InputKey): void {
        // this.output.scrollToBottom();
        this.input.processKey(key);

        if (key.isEnter()) {
            const query: Query =  this.input.getQuery();
            this.addToStorage(query.getText());
            this.executor.execute(query);
            // this.output.scrollToBottom();
            this.input.scroll();
            this.input.clear();
        }

        if (key.isArrowDown()) {
            this.setNextCommand();
        }

        if (key.isArrowUp()) {
            this.setPreviousCommand();
        }
    }


    private async intro(): Promise<void> {
        await this.wait(500);

        this.output.addResponse("Booting...");
        await this.wait(1000);
        this.output.changeLastRow("Booting...OK");

        this.output.addResponse("Loading filesystem...");
        await this.wait(1000);
        this.output.changeLastRow("Loading filesystem...OK");

        this.output.addResponse("Loading libraries...");
        await this.wait(1000);
        this.output.changeLastRow("Loading libraries...OK");

        this.output.addResponse("System ready for commands");

        this.startInput();
        return new Promise<void>((resolve, reject) => {
            resolve();
        });
    }

    private startInput(): void {
        this.started = true;
        this.input.show();
    }

    private async wait(time: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            setTimeout(() => {
                resolve();
            }, time);
        });
    }

    private canProcessKey(event: KeyboardEvent): boolean {
        return this.started && !event.ctrlKey;
    }

    private setPreviousCommand(): void {
        const lastCommandText: string = this.storage.getPrevious();
        this.input.setCommandText(lastCommandText);
    }

    private setNextCommand(): void {
        const nextCommandText: string = this.storage.getNext();
        this.input.setCommandText(nextCommandText);
    }

    private addToStorage(commandText: string): void {
        this.storage.addCommand(commandText);
    }
}