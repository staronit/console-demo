import "reflect-metadata";
import { Container } from "typedi";
import App from './App/App';
import * as WebFont from 'webfontloader';

window.addEventListener('load', (e) => {
    const app: App = Container.get(App);

    WebFont.load({
        google: {
            families: ['Inconsolata']
        },
        active: () => {
            app.start();
        }
    });
});
