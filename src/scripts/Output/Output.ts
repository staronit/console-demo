import {Container, Inject, Service} from "typedi";
import CommandInterface from "../Command/CommandInterface";
import Query from "../Query/Query";

@Service("output")
export default class Output {

    static CLASS_COMMAND = "command";
    static CLASS_ERROR = "error";
    static CLASS_RESPONSE = "response";

    private container: HTMLElement;

    constructor() {
        this.container = <HTMLElement>document.querySelector("#terminal-wrapper .output");
    }

    scrollToBottom(): void {
        window.scrollTo(0, document.body.scrollHeight);
    }

    addCommand(commandText: string): void {
        this.addRow(commandText, Output.CLASS_COMMAND);
    }

    addError(error: string): void {
        this.addRow(error, Output.CLASS_ERROR);
    }

    addResponse(...rows: string[]): void {
        rows = rows.map((row: string): string => {
            return this.filterRow(row);
        });

        for (const row of rows) {
            this.addRow(row, Output.CLASS_RESPONSE);
        }
    }

    addRawResponse(...rows: string[]): void {
        for (const row of rows) {
            this.addRow(row, Output.CLASS_RESPONSE);
        }
    }

    changeLastRow(row: string): void {
        this.container.lastElementChild.innerHTML = row;
    }

    private addRow(row: string, className?: string): void {
        const element = document.createElement("span");
        if (className) {
            element.classList.add(className);
        }

        element.innerHTML = row;
        this.container.appendChild(element);
    }

    private filterRow(row: string): string {
        return row.replace(/\s/g, "&nbsp");
    }
}