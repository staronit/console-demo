export default interface AssetInterface {
    filePath: string;
    fileName: string;
    size: number;
    permissions: string;
    user: string;
    modifiedAt: Date;
}