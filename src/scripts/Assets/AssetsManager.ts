import AssetsManagerInterface from './AssetsManagerInterface';
import AssetInterface from './AssetInterface';
import { assets } from '../Config/assets';
import { Service } from 'typedi';

@Service("assets.assets_manager")
export default class AssetsManager implements AssetsManagerInterface {

    private assets: AssetInterface[];

    constructor() {
        this.assets = assets;
    }

    getAssets(): AssetInterface[] {
        return this.assets;
    }

    fileExists(fileName: string): boolean {
        return this.assets.filter((value) => value.fileName === fileName).length > 0;
    }

    getAsset(fileName: string): AssetInterface {
        for (let asset of this.assets) {
            if (asset.fileName === fileName) {
                return asset;
            }
        }

        return null;
    }
}