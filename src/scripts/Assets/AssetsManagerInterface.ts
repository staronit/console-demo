import AssetInterface from "./AssetInterface";

export default interface AssetsManagerInterface {
    getAssets(): AssetInterface[];
    fileExists(fileName: string): boolean;
    getAsset(fileName: string): AssetInterface;
}