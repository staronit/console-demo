import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandEcho implements CommandInterface {
    private content: string;

    constructor(content: string) {
        this.content = content;
    }

    execute(output: Output): void {
        output.addResponse(`echo says: ${this.content}`);
    }
}