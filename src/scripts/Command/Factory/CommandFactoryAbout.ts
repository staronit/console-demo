import { Service } from "typedi";
import Query from "../../Query/Query";
import CommandAbout from "../CommandAbout";
import CommandInterface from "../CommandInterface";
import CommandFactoryInterface from "./CommandFactoryInterface";

@Service("command.factory.about")
export default class CommandFactoryAbout implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        "about",
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryAbout.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandAbout();
    }
}