import CommandSudo from '../CommandSudo';
import Query from '../../Query/Query';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service } from "typedi";

@Service("command.factory.sudo")
export default class CommandFactorySudo implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        'sudo',
        'su'
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactorySudo.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandSudo();
    }
}