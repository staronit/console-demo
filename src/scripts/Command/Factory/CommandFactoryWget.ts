import CommandWget from '../CommandWget';
import AssetsManagerInterface from '../../Assets/AssetsManagerInterface';
import AssetsManager from '../../Assets/AssetsManager';
import Query from '../../Query/Query';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service, Inject } from "typedi";

@Service("command.factory.wget")
export default class CommandFactoryWget implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        'wget',
        'curl'
    );

    static FILE_NAME_QUERY_PARAMETER = 0;

    private assetsManager: AssetsManagerInterface;

    constructor(
        @Inject("assets.assets_manager") assetsManager: AssetsManager
    ) {
        this.assetsManager = assetsManager;
    }

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryWget.COMMANDS);
    }

    create(query: Query): CommandInterface {
        console.log(query.getParameters());
        const fileName: string = query.getParameter(CommandFactoryWget.FILE_NAME_QUERY_PARAMETER);
        console.log(fileName);
        return new CommandWget(fileName, this.assetsManager.getAsset(fileName));
    }
}