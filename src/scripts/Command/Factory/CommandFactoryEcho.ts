import { Service } from "typedi";
import Query from "../../Query/Query";
import CommandAbout from "../CommandAbout";
import CommandEcho from "../CommandEcho";
import CommandInterface from "../CommandInterface";
import CommandFactoryInterface from "./CommandFactoryInterface";

@Service("command.factory.echo")
export default class CommandFactoryEcho implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        "echo",
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryEcho.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandEcho(query.getParameters().join(" "));
    }
}