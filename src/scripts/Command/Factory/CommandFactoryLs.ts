import AssetsManagerInterface from '../../Assets/AssetsManagerInterface';
import AssetsManager from '../../Assets/AssetsManager';
import CommandLs from '../CommandLs';
import Query from '../../Query/Query';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service, Inject } from "typedi";

@Service("command.factory.ls")
export default class CommandFactoryLs implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        'ls',
        'dir'
    );

    private assetsManager: AssetsManagerInterface;

    constructor(
        @Inject("assets.assets_manager") assetsManager: AssetsManager
    ) {
        this.assetsManager = assetsManager;
    }

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryLs.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandLs(this.assetsManager.getAssets());
    }
}