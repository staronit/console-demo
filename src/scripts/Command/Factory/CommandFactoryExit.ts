import { Service } from "typedi";
import Query from "../../Query/Query";
import CommandExit from "../CommandExit";
import CommandInterface from "../CommandInterface";
import CommandFactoryInterface from "./CommandFactoryInterface";

@Service("command.factory.exit")
export default class CommandFactoryExit implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        "exit",
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryExit.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandExit();
    }
}