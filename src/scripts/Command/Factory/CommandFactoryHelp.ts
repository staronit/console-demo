import Query from '../../Query/Query';
import CommandHelp from '../CommandHelp';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service } from "typedi";

@Service("command.factory.help")
export default class CommandFactoryHelp implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        'help',
        '?'
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryHelp.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandHelp();
    }
}