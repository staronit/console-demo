import {Container, Inject, Service, Token} from "typedi";
import Query from "../../Query/Query";
import CommandInterface from "../CommandInterface";
import CommandFactoryAbout from "./CommandFactoryAbout";
import CommandFactoryEcho from "./CommandFactoryEcho";
import CommandFactoryExit from "./CommandFactoryExit";
import CommandFactoryHelp from "./CommandFactoryHelp";
import CommandFactoryInterface from "./CommandFactoryInterface";
import CommandFactoryKill from "./CommandFactoryKill";
import CommandFactoryLs from "./CommandFactoryLs";
import CommandFactoryNotFound from "./CommandFactoryNotFound";
import CommandFactorySudo from "./CommandFactorySudo";
import CommandFactoryWget from "./CommandFactoryWget";

@Service("command.factory")
export default class CommandFactoryChain {

    private commandFactoryNotFound: CommandFactoryInterface;
    private factories: CommandFactoryInterface[];

    constructor(
        @Inject("command.factory.not_found") commandFactoryNotFound: CommandFactoryNotFound,
        @Inject("command.factory.help") commandFactoryHelp: CommandFactoryHelp,
        @Inject("command.factory.exit") commandFactoryExit: CommandFactoryExit,
        @Inject("command.factory.about") commandFactoryAbout: CommandFactoryAbout,
        @Inject("command.factory.echo") commandFactoryEcho: CommandFactoryEcho,
        @Inject("command.factory.sudo") commandFactorySudo: CommandFactorySudo,
        @Inject("command.factory.kill") commandFactoryKill: CommandFactoryKill,
        @Inject("command.factory.ls") commandFactoryLs: CommandFactoryLs,
        @Inject("command.factory.wget") commandFactoryWget: CommandFactoryWget,
    ) {
        this.commandFactoryNotFound = commandFactoryNotFound;
        this.factories = new Array<CommandFactoryInterface>(
            commandFactoryHelp,
            commandFactoryExit,
            commandFactoryAbout,
            commandFactoryEcho,
            commandFactorySudo,
            commandFactoryKill,
            commandFactoryLs,
            commandFactoryWget,
        );
    }

    create(query: Query): CommandInterface {
        const factory: CommandFactoryInterface = this.getFactory(query);
        return factory.create(query);
    }

    private getFactory(query: Query): CommandFactoryInterface {
        for (const factory of this.factories) {
            if (factory.canCreate(query)) {
                return factory;
            }
        }

        return this.commandFactoryNotFound;
    }
}