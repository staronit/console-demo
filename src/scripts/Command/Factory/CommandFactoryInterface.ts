import Query from "../../Query/Query";
import CommandInterface from "../CommandInterface";

export default interface CommandFactoryInterface {
    canCreate(query: Query): boolean;
    create(query: Query): CommandInterface;
}