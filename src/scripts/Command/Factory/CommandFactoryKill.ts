import CommandKill from '../CommandKill';
import Query from '../../Query/Query';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service } from "typedi";

@Service("command.factory.kill")
export default class CommandFactoryKill implements CommandFactoryInterface {

    static COMMANDS = new Array<string>(
        'kill',
        'killall'
    );

    canCreate(query: Query): boolean {
        return query.matchCommand(CommandFactoryKill.COMMANDS);
    }

    create(query: Query): CommandInterface {
        return new CommandKill();
    }
}