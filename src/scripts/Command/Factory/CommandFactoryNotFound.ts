import CommandNotFound from '../CommandNotFound';
import Query from '../../Query/Query';
import CommandInterface from '../CommandInterface';
import CommandFactoryInterface from './CommandFactoryInterface';
import { Service } from "typedi";

@Service("command.factory.not_found")
export default class CommandFactoryNotFound implements CommandFactoryInterface {

    canCreate(query: Query): boolean {
        return true;
    }

    create(query: Query): CommandInterface {
        return new CommandNotFound(query.getText());
    }
}