import AssetInterface from "../Assets/AssetInterface";
import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandWget implements CommandInterface {

    private asset: AssetInterface;
    private fileName: string;

    constructor(fileName: string, asset?: AssetInterface) {
        this.fileName = fileName;
        this.asset = asset;
    }

    execute(output: Output): void {
        if (!this.asset) {
            output.addError(`file <i>${this.fileName}</i> not found`);
            return;
        }

        window.open(this.asset.filePath);
    }
}