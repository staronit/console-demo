import AssetInterface from "../Assets/AssetInterface";
import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandLs implements CommandInterface {
    private assets: AssetInterface[];

    constructor(assets: AssetInterface[]) {
        this.assets = assets;
    }

    execute(output: Output): void {
        output.addResponse(`total ${this.assets.length}`);
        for (let asset of this.assets) {
            output.addResponse(this.generateOutputRow(asset));
        }
    }

    private generateOutputRow(asset: AssetInterface): string {
        const date: string = `${asset.modifiedAt.toDateString()} ${asset.modifiedAt.toLocaleTimeString()}`;
        return `${asset.permissions}    ${asset.user}   ${asset.size}K  ${date}     ${asset.fileName}`;
    }
}