import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandNotFound implements CommandInterface {
    private commandText: string;

    constructor(commandText: string) {
        this.commandText = commandText;
    }

    execute(output: Output): void {
        if (this.commandText) {
            output.addError(`${this.commandText}: command not found, please type <i>help</i> for help ;-)`);
        }
    }
}