import Output from '../Output/Output';
import CommandInterface from './CommandInterface';

export default class CommandSudo implements CommandInterface {
    execute(output: Output): void {
        output.addRawResponse(
            `<img src="/assets/img/sudo.jpg" alt="sudo"/>`
        );
        output.addError('there is no root');
    }
}