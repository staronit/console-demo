import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandExit implements CommandInterface {
    execute(output: Output): void {
        window.close();
    }
}