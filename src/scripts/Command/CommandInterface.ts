import Output from "../Output/Output";

export default interface CommandInterface {
    execute(output: Output): void;
}