import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandAbout implements CommandInterface {
    execute(output: Output): void {
        output.addResponse(
            `Hi, my name is Jacek Staroń and i am very glad you get here :-)`,
            ` `,
            `I like being a programmer. I like to solve problems. I like to design and create new`,
            `things just as I like to analyze and look for bugs in the old. I like to discover`,
            `other technologies and capabilities. I like to learn. I like challenges. I like blue color`,
            `I like good audio equipment and music. I like to play football. I like cooking and Italian cars.`,
            ` `,
        );
        output.addRawResponse(
            `If you want to contact, please feel free to email me @ <a href="mailto:kontakt@jacekstaron.pl">kontakt@jacekstaron.pl</a>`,
            `Or just see me @ <a target="_blank" href="https://www.linkedin.com/in/jacek-staron/"><img src="/assets/img/linkedin.png" alt="linkedin"/></a>`,
        );
    }
}