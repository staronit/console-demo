import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandHelp implements CommandInterface {
    execute(output: Output): void {
        output.addResponse(
            `Welcome @ jacekstaron.pl v0.6.1, interactive website`,
            ` `,
            `Usage:`,
            `   help        <i>prints this help</i>`,
            `   about       <i>prints info about me</i>`,
            `   echo &lt;text&gt; <i>prints input</i>`,
            `   ls          <i>lists all files</i>`,
            `   wget &lt;file&gt; <i>download file</i>`,
            `   exit        <i>closes application</i>`,
            ` `,
        );
    }
}