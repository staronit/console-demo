import Output from "../Output/Output";
import CommandInterface from "./CommandInterface";

export default class CommandKill implements CommandInterface {
    execute(output: Output): void {
        output.addRawResponse(`who?`);
    }
}