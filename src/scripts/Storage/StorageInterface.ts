import Query from "../Query/Query";

export default interface StorageInterface {
    addCommand(commandText: string): void;
    getPrevious(): string;
    getNext(): string;

}