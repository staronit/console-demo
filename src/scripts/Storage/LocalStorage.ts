import { Service } from "typedi";
import Query from "../Query/Query";
import StorageInterface from "./StorageInterface";

@Service("storage.local_storage")
export default class LocalStorage implements StorageInterface {

    private index: number;

    constructor() {
        this.index = localStorage.length - 1;
    }

    addCommand(commandText: string): void {
        if (this.isLastCommandThisSame(commandText)) {
            return;
        }

        this.index++;
        localStorage.setItem(
            this.getKey(this.index),
            commandText,
        );

    }

    getPrevious(): string {
        const key: string = localStorage.key(this.index);
        if (key !== null) {
            this.index--;
            return localStorage.getItem(key);
        }

        return "";
    }

    getNext(): string {
        const key: string = localStorage.key(this.index + 1);
        if (key !== null) {
            this.index++;
            return localStorage.getItem(key);
        }

        return "";
    }


    private isLastCommandThisSame(commandText: string): boolean {
        const lastCommandText = localStorage.key(this.index);
        return lastCommandText === commandText;
    }


    private getKey(index: number): string {
        return `query-${index}`;
    }
}