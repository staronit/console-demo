import { Service } from "typedi";
import Query from "../Query";

@Service("query.factory")
export default class QueryFactory {
    create(commandText: string): Query {
        const [ command, ...parameters ] = commandText.split(" ");
        return new Query(commandText, command, parameters);
    }
}