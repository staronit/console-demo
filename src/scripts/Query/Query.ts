export default class Query {
    private text: string;
    private command: string;
    private parameters: string[];

    constructor(text: string, command: string, parameters: string[] = []) {
        this.text = text;
        this.command = command.toLowerCase();
        this.parameters = parameters;
    }

    getText(): string {
        return this.text;
    }

    matchCommand(commands: string[]): boolean {
        return commands.indexOf(this.command) > -1;
    }

    getCommand(): string {
        return this.command;
    }

    getParameters(): string[] {
        return this.parameters;
    }

    getParameter(index: number, defaultValue: string = ""): string {
        return this.parameters.length > index ? this.parameters[index] : defaultValue;
    }
}