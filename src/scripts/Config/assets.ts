import AssetInterface from "../Assets/AssetInterface";

const assets: AssetInterface[] = [
    {
        filePath: "assets/pdf/cv_jacek_staron.pdf",
        fileName: "cv_jacek_staron.pdf",
        size: 240,
        permissions: "-r--r--r--",
        user: "jstaron",
        modifiedAt: new Date(2017, 11, 8, 19, 37),
    },
    {
        filePath: "https://bitbucket.org/staronit/console-demo/get/48d19791542c.zip",
        fileName: "source.zip",
        size: 240,
        permissions: "-r--r--r--",
        user: "jstaron",
        modifiedAt: new Date(2017, 11, 8, 21, 10),
    },
];

export { assets };

