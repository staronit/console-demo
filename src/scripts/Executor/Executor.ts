import {Container, Inject, Service} from "typedi";
import CommandInterface from "../Command/CommandInterface";
import CommandFactoryChain from "../Command/Factory/CommandFactoryChain";
import CommandFactoryHelp from "../Command/Factory/CommandFactoryHelp";
import CommandFactoryInterface from "../Command/Factory/CommandFactoryInterface";
import Output from "../Output/Output";
import Query from "../Query/Query";

@Service("executor")
export default class Executor {
    private output: Output;
    private commandFactory: CommandFactoryChain;

    constructor(
        @Inject("output") outout: Output,
        @Inject("command.factory") commandFactory: CommandFactoryChain,
    ) {
        this.output = outout;
        this.commandFactory = commandFactory;
    }

    execute(query: Query) {
        this.output.addCommand(query.getText());

        const command: CommandInterface = this.commandFactory.create(query);
        command.execute(this.output);
    }

}